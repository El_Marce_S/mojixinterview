import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:interview/functions.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Compare Two Strings',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.amber,
      ),
      home: MyHomePage(title: 'Comparar dos Versiones'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  var _versionA;
  var _versionB;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: Colors.black),),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Container(
                width: 600,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                          hintText:
                              "Ingrese Una version en el Formato #.#.# Ejm. 1.2.3"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Ingresa una version';
                        }
                        setState(() {
                          _versionA = value;
                        });
                      },
                    ),
                    TextFormField(
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                          hintText:
                              "Ingrese Una version en el Formato #.#.# Ejm. 1.2.3"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Ingresa una version';
                          // ignore: missing_return
                        } else {
                          setState(
                            () {
                              _versionB = value;
                            },
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            var _result = CompareVersions(_versionA, _versionB);
            if (_result == 0) {
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('Amabas VErsiones son Iguales')));
            } else if (_result == 1) {
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('La Primera Version es mayor')));
            } else if (_result == -1) {
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('La Segunda Version es mayor')));
            }
          } else {
            print('vacio');
          }
        },
        child: Text('RUN'
            ''),
        tooltip: 'Comparar',
      ),
    );
  }
}
